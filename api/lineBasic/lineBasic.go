// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package lineBasic

import (
	"context"

	"gf-demo/api/lineBasic/v1"
)

type ILineBasicV1 interface {
	CheckLineCode(ctx context.Context, req *v1.CheckLineCodeReq) (res *v1.CheckLineCodeRes, err error)
	CheckLineName(ctx context.Context, req *v1.CheckLineNameReq) (res *v1.CheckLineNameRes, err error)
	Create(ctx context.Context, req *v1.CreateReq) (res *v1.CreateRes, err error)
	Delete(ctx context.Context, req *v1.DeleteReq) (res *v1.DeleteRes, err error)
	Get(ctx context.Context, req *v1.GetReq) (res *v1.GetRes, err error)
	GetList(ctx context.Context, req *v1.GetListReq) (res *v1.GetListRes, err error)
	Update(ctx context.Context, req *v1.UpdateReq) (res *v1.UpdateRes, err error)
}
