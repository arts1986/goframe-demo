package v1

import "github.com/gogf/gf/v2/frame/g"

type CheckLineNameReq struct {
	g.Meta   `path:"/lineBasic/check-lineName" method:"post" tags:"LineBasicService" summary:"查重LineName"`
	LineName string `v:"required"`
}
type CheckLineNameRes struct {
	OK bool `dc:"True-LineName未被使用; False-LineName已被使用"`
}
