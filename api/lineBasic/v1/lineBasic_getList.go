package v1

import (
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

type GetListReq struct {
	g.Meta `path:"/lineBasic/getList" method:"get" tags:"LineBasicService" summary:"获取所有线路"`
}
type GetListRes struct {
	Result gdb.Result
}
