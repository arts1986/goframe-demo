package v1

import (
	"database/sql"
	"github.com/gogf/gf/v2/frame/g"
)

type DeleteReq struct {
	g.Meta `path:"/lineBasic/delete" method:"delete" tags:"LineBasicService" summary:"删除线路"`
	Id     int `v:"required"`
}
type DeleteRes struct {
	Result sql.Result
}
