// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package locale

import (
	"context"

	"gf-demo/api/locale/v1"
)

type ILocaleV1 interface {
	SetLocale(ctx context.Context, req *v1.SetLocaleReq) (res *v1.SetLocaleRes, err error)
}
