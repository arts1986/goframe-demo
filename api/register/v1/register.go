package v1

import "github.com/gogf/gf/v2/frame/g"

type RegisterReq struct {
	g.Meta    `path:"/register" method:"post" tags:"UserService" summary:"注册一个新的账号"`
	Name      string `v:"required|length:6,16"`
	Password  string `v:"required|length:6,16"`
	Password2 string `v:"required|length:6,16|same:Password"`
	CreateBy  int    `v:"required"`
}
type RegisterRes struct{}
