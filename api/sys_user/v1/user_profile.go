package v1

import (
	"gf-demo/internal/model/entity"
	"github.com/gogf/gf/v2/frame/g"
)

type ProfileReq struct {
	g.Meta `path:"/user/profile" method:"get" tags:"UserService" summary:"获取当前账户信息"`
}
type ProfileRes struct {
	*entity.SysUser
}
