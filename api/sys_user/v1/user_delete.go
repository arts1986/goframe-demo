package v1

import (
	"database/sql"
	"github.com/gogf/gf/v2/frame/g"
)

type DeleteReq struct {
	g.Meta `path:"/user/delete" method:"delete" tags:"UserService" summary:"删除用户"`
	Id     int `v:"required"`
}
type DeleteRes struct {
	Result sql.Result
}
