package v1

import (
	"database/sql"
	"github.com/gogf/gf/v2/frame/g"
)

type UpdateReq struct {
	g.Meta   `path:"/user/update" method:"put" tags:"UserService" summary:"修改用户"`
	Id       int    `v:"required"`
	Name     string `v:"required"`
	Password string `v:"required"`
}
type UpdateRes struct {
	Result sql.Result
}
