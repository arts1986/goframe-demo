package v1

import (
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

type GetReq struct {
	g.Meta `path:"/user/get" method:"get" tags:"UserService" summary:"查询用户"`
	Name   string
}
type GetRes struct {
	Result gdb.Result
}
