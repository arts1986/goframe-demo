package v1

import "github.com/gogf/gf/v2/frame/g"

type CheckNameReq struct {
	g.Meta `path:"/user/check-name" method:"post" tags:"UserService" summary:"查重Name"`
	Name   string `v:"required"`
}
type CheckNameRes struct {
	OK bool `dc:"True-Name未被使用; False-Name已被使用"`
}
