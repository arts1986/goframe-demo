package v1

import (
	"database/sql"
	"github.com/gogf/gf/v2/frame/g"
)

type CreateReq struct {
	g.Meta   `path:"/user/create" method:"post" tags:"UserService" summary:"新增用户"`
	Name     string `v:"required"`
	Password string `v:"required"`
}
type CreateRes struct {
	Result sql.Result
}
