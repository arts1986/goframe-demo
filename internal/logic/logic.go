// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package logic

import (
	_ "gf-demo/internal/logic/bizctx"
	_ "gf-demo/internal/logic/cache"
	_ "gf-demo/internal/logic/lineBasic"
	_ "gf-demo/internal/logic/locale"
	_ "gf-demo/internal/logic/middleware"
	_ "gf-demo/internal/logic/session"
	_ "gf-demo/internal/logic/user"
	_ "gf-demo/internal/logic/util"
)
