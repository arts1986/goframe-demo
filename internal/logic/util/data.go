package util

// 判断数组是否包含某个元素
// 根据数组元素个数进行方法选择
// 15 个以下，无论是 int 还是 string，建议用 slice
// 15 ~ 100 个，尽量转为 int ，如果不能转为 int 就用 map
// 100 个以上，果断用 map，int 和 string 都差不多

// ContainsGeneric 判断数组是否包含某个元素，遍历列表
func ContainsGeneric[T comparable](slice []T, element T) bool {
	for _, e := range slice {
		if e == element {
			return true
		}
	}

	return false
}

// ContainsMapStr 判断数组是否包含某个元素，转换成 map 判断
func ContainsMapStr(mp map[string]struct{}, element string) bool {
	_, ok := mp[element]
	return ok
}
