// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SysUser is the golang structure for table sys_user.
type SysUser struct {
	Id         int64       `json:"id"         description:"id"`
	Name       string      `json:"name"       description:"用户名"`
	Pwd        string      `json:"pwd"        description:"用户登录密码"`
	CreateBy   int64       `json:"createBy"   description:"创建人"`
	UpdateBy   int64       `json:"updateBy"   description:"修改人"`
	CreateTime *gtime.Time `json:"createTime" description:"创建时间"`
	UpdateTime *gtime.Time `json:"updateTime" description:"修改时间"`
	DeleteTime *gtime.Time `json:"deleteTime" description:"软删除时间"`
}
