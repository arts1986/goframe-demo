// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// DiAccLineBasic is the golang structure for table di_acc_line_basic.
type DiAccLineBasic struct {
	Id         int64       `json:"id"         description:"id"`
	LineCode   string      `json:"lineCode"   description:"线路编号"`
	LineName   string      `json:"lineName"   description:"线路名称"`
	CreateBy   int64       `json:"createBy"   description:"创建人"`
	UpdateBy   int64       `json:"updateBy"   description:"更新人"`
	CreateTime *gtime.Time `json:"createTime" description:"创建时间"`
	UpdateTime *gtime.Time `json:"updateTime" description:"更新时间"`
	DeleteTime *gtime.Time `json:"deleteTime" description:"软删除时间"`
}
