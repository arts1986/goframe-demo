// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SysUser is the golang structure of table sys_user for DAO operations like Where/Data.
type SysUser struct {
	g.Meta     `orm:"table:sys_user, do:true"`
	Id         interface{} // id
	Name       interface{} // 用户名
	Pwd        interface{} // 用户登录密码
	CreateBy   interface{} // 创建人
	UpdateBy   interface{} // 修改人
	CreateTime *gtime.Time // 创建时间
	UpdateTime *gtime.Time // 修改时间
	DeleteTime *gtime.Time // 软删除时间
}
