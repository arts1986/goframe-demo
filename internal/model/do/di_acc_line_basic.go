// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// DiAccLineBasic is the golang structure of table di_acc_line_basic for DAO operations like Where/Data.
type DiAccLineBasic struct {
	g.Meta     `orm:"table:di_acc_line_basic, do:true"`
	Id         interface{} // id
	LineCode   interface{} // 线路编号
	LineName   interface{} // 线路名称
	CreateBy   interface{} // 创建人
	UpdateBy   interface{} // 更新人
	CreateTime *gtime.Time // 创建时间
	UpdateTime *gtime.Time // 更新时间
	DeleteTime *gtime.Time // 软删除时间
}
