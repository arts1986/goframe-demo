// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"gf-demo/internal/model"
	"time"

	"github.com/gogf/gf/v2/container/gvar"
)

type (
	IGCacheUtil interface {
		// SetWithNoExpire 设置缓存，不过期
		SetWithNoExpire(ctx context.Context, keyValue model.GCacheKeyValue) (err error)
		// SetIfNotExist 当键名不存在时写入，设置过期时间{second}秒
		SetIfNotExist(ctx context.Context, key interface{}, value interface{}, second time.Duration) (b bool, err error)
		// Get 获取缓存值
		Get(ctx context.Context, key interface{}) (value *gvar.Var, err error)
		// GetOrSet 获取指定键值，如果不存在时写入，并返回键值
		GetOrSet(ctx context.Context, key interface{}, value interface{}) (valueR *gvar.Var, err error)
		// KeyValueList 打印当前的键值对
		KeyValueList(ctx context.Context) (data map[interface{}]interface{}, err error)
		// KeyList 打印当前的键名列表
		KeyList(ctx context.Context) (keys []interface{}, err error)
		// ValueList 打印当前的键值列表
		ValueList(ctx context.Context) (values []interface{}, err error)
		// Count 获取缓存大小
		Count(ctx context.Context) (size int, err error)
		// ExistKey 缓存中是否存在指定键名
		ExistKey(ctx context.Context, key interface{}) (b bool, err error)
		// RemoveKey 删除并返回被删除的键值
		RemoveKey(ctx context.Context, key interface{}) (removedValue *gvar.Var, err error)
		// Close 关闭缓存对象，让GC回收资源
		Close(ctx context.Context) (err error)
	}
)

var (
	localGCacheUtil IGCacheUtil
)

func GCacheUtil() IGCacheUtil {
	if localGCacheUtil == nil {
		panic("implement not found for interface IGCacheUtil, forgot register?")
	}
	return localGCacheUtil
}

func RegisterGCacheUtil(i IGCacheUtil) {
	localGCacheUtil = i
}
