// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"database/sql"
	"gf-demo/internal/model"
	"gf-demo/internal/model/entity"

	"github.com/gogf/gf/v2/database/gdb"
)

type (
	IUser interface {
		Create(ctx context.Context, in model.UserCreateInput) (res sql.Result, err error)
		Delete(ctx context.Context, in model.UserDeleteInput) (sql.Result, error)
		Count(ctx context.Context) (int, error)
		Update(ctx context.Context, in model.UserUpdateInput) (sql.Result, error)
		GetList(ctx context.Context) (gdb.Result, error)
		Get(ctx context.Context, in model.UserGetInput) (gdb.Result, error)
		// IsSignedIn checks and returns whether current user is already signed-in.
		IsSignedIn(ctx context.Context) bool
		// SignIn creates session for given user account.
		SignIn(ctx context.Context, in model.UserSignInInput) error
		// SignOut removes the session for current signed-in user.
		SignOut(ctx context.Context) error
		// GetProfile retrieves and returns current user info in session.
		GetProfile(ctx context.Context) *entity.SysUser
		// IsNameAvailable checks and returns given Name is available for signing up.
		IsNameAvailable(ctx context.Context, Name string) (bool, error)
		// IsIdAvailable checks and returns given id is available.
		IsIdAvailable(ctx context.Context, id int) (bool, error)
	}
)

var (
	localUser IUser
)

func User() IUser {
	if localUser == nil {
		panic("implement not found for interface IUser, forgot register?")
	}
	return localUser
}

func RegisterUser(i IUser) {
	localUser = i
}
