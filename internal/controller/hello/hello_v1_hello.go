package hello

import (
	"context"
	"gf-demo/api/hello/v1"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

func (c *ControllerV1) Hello(ctx context.Context, req *v1.HelloReq) (*v1.HelloRes, error) {
	g.RequestFromCtx(ctx).Response.Writeln("Hello World!")
	return nil, gerror.NewCode(gcode.CodeOK)
}
