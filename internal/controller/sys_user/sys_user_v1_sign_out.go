package sys_user

import (
	"context"
	"gf-demo/internal/service"

	"gf-demo/api/sys_user/v1"
)

func (c *ControllerV1) SignOut(ctx context.Context, req *v1.SignOutReq) (res *v1.SignOutRes, err error) {
	err = service.User().SignOut(ctx)
	return
}
