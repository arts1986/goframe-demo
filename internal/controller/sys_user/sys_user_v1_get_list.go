package sys_user

import (
	"context"
	v1 "gf-demo/api/sys_user/v1"
	"gf-demo/internal/service"
)

func (c *ControllerV1) GetList(ctx context.Context, req *v1.GetListReq) (*v1.GetListRes, error) {
	result, err := service.User().GetList(ctx)
	res := &v1.GetListRes{
		Result: result,
	}
	return res, err
}
