package sys_user

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/sys_user/v1"
)

func (c *ControllerV1) Update(ctx context.Context, req *v1.UpdateReq) (*v1.UpdateRes, error) {
	result, err := service.User().Update(ctx, model.UserUpdateInput{
		Id:       req.Id,
		Name:     req.Name,
		Password: req.Password,
	})
	res := &v1.UpdateRes{
		Result: result,
	}
	return res, err
}
