package sys_user

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/sys_user/v1"
)

func (c *ControllerV1) Get(ctx context.Context, req *v1.GetReq) (*v1.GetRes, error) {
	result, err := service.User().Get(ctx, model.UserGetInput{
		Name: req.Name,
	})
	res := &v1.GetRes{
		Result: result,
	}
	return res, err
}
