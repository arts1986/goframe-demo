package sys_user

import (
	"context"
	"gf-demo/internal/service"
	"github.com/gogf/gf/v2/errors/gcode"

	"github.com/gogf/gf/v2/errors/gerror"

	"gf-demo/api/sys_user/v1"
)

func (c *ControllerV1) CheckName(ctx context.Context, req *v1.CheckNameReq) (*v1.CheckNameRes, error) {
	available, err := service.User().IsNameAvailable(ctx, req.Name)
	if err != nil {
		return nil, err
	}
	res := &v1.CheckNameRes{
		OK: available,
	}
	err = gerror.NewCodef(gcode.CodeOK, `Name "%s" 可用`, req.Name)
	if !available {
		err = gerror.NewCodef(gcode.CodeBusinessValidationFailed, `Name "%s" 已被使用`, req.Name)
	}
	return res, err
}
