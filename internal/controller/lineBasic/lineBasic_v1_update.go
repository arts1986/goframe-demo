package lineBasic

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/lineBasic/v1"
)

func (c *ControllerV1) Update(ctx context.Context, req *v1.UpdateReq) (*v1.UpdateRes, error) {
	result, err := service.LineBasic().Update(ctx, model.LineBasicUpdateInput{
		Id:       req.Id,
		LineCode: req.LineCode,
		LineName: req.LineName,
	})
	res := &v1.UpdateRes{
		Result: result,
	}
	return res, err
}
