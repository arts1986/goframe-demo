package lineBasic

import (
	"context"
	"gf-demo/internal/service"
	"github.com/gogf/gf/v2/errors/gcode"

	"github.com/gogf/gf/v2/errors/gerror"

	"gf-demo/api/lineBasic/v1"
)

func (c *ControllerV1) CheckLineCode(ctx context.Context, req *v1.CheckLineCodeReq) (*v1.CheckLineCodeRes, error) {
	available, err := service.LineBasic().IsLineCodeAvailable(ctx, req.LineCode)
	if err != nil {
		return nil, err
	}
	res := &v1.CheckLineCodeRes{
		OK: available,
	}
	err = gerror.NewCodef(gcode.CodeOK, `LineCode "%d" 可用`, req.LineCode)
	if !available {
		err = gerror.NewCodef(gcode.CodeBusinessValidationFailed, `LineCode "%d" 已被使用`, req.LineCode)
	}
	return res, err
}
