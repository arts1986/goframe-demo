package lineBasic

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/lineBasic/v1"
)

func (c *ControllerV1) Create(ctx context.Context, req *v1.CreateReq) (*v1.CreateRes, error) {
	result, err := service.LineBasic().Create(ctx, model.LineBasicCreateInput{
		LineCode: req.LineCode,
		LineName: req.LineName,
	})
	res := &v1.CreateRes{
		Result: result,
	}
	return res, err
}
