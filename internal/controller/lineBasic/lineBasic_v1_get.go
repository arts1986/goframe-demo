package lineBasic

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"

	"gf-demo/api/lineBasic/v1"
)

func (c *ControllerV1) Get(ctx context.Context, req *v1.GetReq) (*v1.GetRes, error) {
	result, err := service.LineBasic().Get(ctx, model.LineBasicGetInput{
		LineCode: req.LineCode,
		LineName: req.LineName,
	})
	res := &v1.GetRes{
		Result: result,
	}
	return res, err
}
