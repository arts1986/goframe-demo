package lineBasic

import (
	"context"
	v1 "gf-demo/api/lineBasic/v1"
	"gf-demo/internal/service"
)

func (c *ControllerV1) GetList(ctx context.Context, req *v1.GetListReq) (*v1.GetListRes, error) {
	result, err := service.LineBasic().GetList(ctx)
	res := &v1.GetListRes{
		Result: result,
	}
	return res, err
}
