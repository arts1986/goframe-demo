package register

import (
	"context"
	"gf-demo/internal/model"
	"gf-demo/internal/service"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/i18n/gi18n"

	"gf-demo/api/register/v1"
)

func (c *ControllerV1) Register(ctx context.Context, req *v1.RegisterReq) (*v1.RegisterRes, error) {
	_, err := service.User().Create(ctx, model.UserCreateInput{
		Name:     req.Name,
		Password: req.Password,
	})
	if err != nil {
		return nil, err
	}
	return nil, gerror.NewCode(gcode.CodeOK, gi18n.T(ctx, "#{}"))
}
